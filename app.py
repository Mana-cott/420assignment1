import math
import random
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = float(request.form['dividend'])
    divisor = float(request.form['divisor'])
    #Determine if the dividend is odd or even or a multiple of 4
    msg = ""
    if dividend % 2 == 0:
        if dividend % 4 == 0:
            msg += "The dividend is even and a multiple of 4."
        else:
            msg += "The dividend is even and not a multiple of 4."
    else:
        msg += "The dividend is odd and not a multiple of 4."
    #Determine if the dividend divides evenly by the divisor
    if dividend % divisor == 0:
        msg += " The dividend also divides evenly by the divisor."
    else:
        msg += " The dividen also does not divide evenly by the divisor."
    #returning both messages
    return msg

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = str(request.form['name'])
    grade = float(request.form['grade'])
    msg = ""
    if grade < 0.00 or grade > 100.00:
        msg += "The value entered for graded is outside the range of 0-100, please provide the information again."
    else:
        msg += name + " :"
        if grade >= 95.00:
            msg += " A"
        elif grade >= 80.00 and grade < 95.00:
            msg += " B"
        elif grade >= 70.00 and grade <= 79.00:
            msg += " C"
        elif grade >= 60.00 and grade <= 69.00:
            msg += " D"
        elif grade <= 59.00:
            msg += " F"
    return msg

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = float(request.form['value'])
    msg = ""
    if type(value) is float:
        msg += "Square Root of " + str(value) + ": " + str(math.sqrt(value))
    else:
        msg += "Invalid type."
    return msg

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])
    isNotEqualToChosen = True
    msg = "Chosen--Random<br/>"
    counter = 0
    while isNotEqualToChosen:
        random_number = random.randrange(1,100)
        if random_number == number:
            msg += "["+ str(number) +"] ["+ str(random_number) +"]<br/>"
            isNotEqualToChosen = False
        else:
            msg += "["+ str(number) +"] ["+ str(random_number) +"]<br/>"
            counter += 1
    msg += "It took " + str(counter) + " tries to match!"
    return msg

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = int(request.form['number'])
    msg = "Chosen Number: " + str(number) + "<br/>"
    first = 1
    second = first
    while first < number:
        msg += str(first) + " - "
        tmp_first = first
        first = second
        second = tmp_first + second
    return msg + "END!"